This module helps distribute moderation responsibilities across members of
a team. It divides incoming submissions into queues, rather than presenting a
single queue of new submissions, so that each team member can get to 'inbox 0'
in their assigned queue.

## setup
1. create content type
2. enable module
3. configure settings at /admin/config/moderation_team/settings
  - Content Type: Select content type for which submissions should be queued
  - Maximum Queues: Set the number of queues to be generated
  - Sync existing content: Select to add existing nodes of specified content
  type into queues (leaving unselected will result in empty queues even when
  submissions of the specified type are already available)
4. set permissions for the roles that should be able to
  a. Access admin config settings
  b. Access moderation queue list

## use
1. view queue summary
  This page lists each queue along with the number of nodes; click to visit
  the queue.
2. view new submissions in a given queue
3. remove moderated submissions from queue contents ()

## The minimum viable product (MVP) of this module involves:

### log submission  
Requirement: An append-only record of each submission of each content type.  
Proposed solution: A custom database table to store basic information about a
submission as it is received.  
NB: the record could potentially also be used to report the number of
submissions received (reporting requirements in a separate ticket and not part
of this MVP).

### divide submissions into queues  
Requirement: A method for dividing submissions evenly across multiple lists to
allow each list to grow as submissions are received.  
Proposed solution: A record in the custom database table assigning 1 of 50
queue designations sequentially to a submission as it is received.  
NB: the working assumption is that once a queue designation is assigned to a
submission, the designation will not change.

### display queues  
Requirement: A method for displaying individual lists of questions to users.  
Proposed solution: A View to render each queue.  
NB: sorting and filtering will eventually be needed for queue items.
