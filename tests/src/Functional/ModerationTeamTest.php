<?php

namespace Drupal\Tests\moderation_team\Functional;

use Drupal\Component\Utility\Unicode;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItem;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\Tests\BrowserTestBase;
use Drupal\user\UserInterface;

/**
 * Functional tests for the Moderation Team module.
 *
 * @group moderation_team
 */
class ModerationTeamTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'moderation_team_test',
    'datetime',
  ];

  /**
   * The moderator1 account.
   *
   * @var \Drupal\user\UserInterface
   */
  private UserInterface $moderator1;

  /**
   * The moderator2 account.
   *
   * @var \Drupal\user\UserInterface
   */
  private UserInterface $moderator2;

  /**
   * The admin account.
   *
   * @var \Drupal\user\UserInterface
   */
  private UserInterface $admin;


  /**
   * Config datetime field name.
   *
   * @var string
   */
  private string $datetimeFiledName;

  /**
   * {@inheritdoc}
   */
  protected function setUp($import_test_views = TRUE, $modules = ['moderation_team_test']): void {
    parent::setUp($import_test_views, $modules);

    // Create a config datetime field.
    $this->datetimeFiledName = $this->randomMachineName();
    $field_label = Unicode::ucfirst($this->randomMachineName());
    $field_storage = FieldStorageConfig::create([
      'field_name' => $this->datetimeFiledName,
      'entity_type' => 'node',
      'type' => 'datetime',
      'settings' => ['datetime_type' => DateTimeItem::DATETIME_TYPE_DATETIME],
    ]);
    $field_storage->save();
    $field = FieldConfig::create([
      'field_storage' => $field_storage,
      'label' => $field_label,
      'bundle' => 'article',
      'description' => 'Description for ' . $field_label,
      'required' => TRUE,
    ]);
    $field->save();

    // Create articles.
    $articles = [
      [
        'title' => 'article 1',
        'created' => strtotime('7:00'),
        $this->datetimeFiledName => DrupalDateTime::createFromTimestamp(strtotime('7:00'))->format(DateTimeItem::DATETIME_STORAGE_FORMAT),
      ],
      [
        'title' => 'article 2',
        'created' => strtotime('8:15'),
      ],
      [
        'title' => 'article 3',
        'created' => strtotime('9:29'),
      ],
      [
        'title' => 'article 4',
        'created' => strtotime('10:30'),
        $this->datetimeFiledName => DrupalDateTime::createFromTimestamp(strtotime('10:30'))->format(DateTimeItem::DATETIME_STORAGE_FORMAT),
      ],
      [
        'title' => 'article 5',
        'created' => strtotime('11:45'),
      ],
      [
        'title' => 'article 6',
        'created' => strtotime('12:59'),
      ],
    ];

    foreach ($articles as $values) {
      $this->createNode([
        'type' => 'article',
      ] + $values
      );
    }

    // Create test moderators.
    $this->moderator1 = $this->drupalCreateUser(values: ['roles' => ['article_moderator']]);
    $this->moderator2 = $this->drupalCreateUser(values: ['roles' => ['article_moderator']]);

    // The admin user.
    $this->admin = $this->drupalCreateUser([
      'access administration pages',
      'administer views',
    ]);

  }

  /**
   * Test a moderaiton queue.
   */
  public function testModerationTeamQueue(): void {
    // The moderator1 should access to only the articles created in the first
    // half hour.
    $this->drupalLogin($this->moderator1);
    $this->drupalGet('article-moderation-queue');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->linkExists('article 1');
    $this->assertSession()->linkExists('article 2');
    $this->assertSession()->linkExists('article 3');
    $this->assertSession()->linkNotExists('article 4');
    $this->assertSession()->linkNotExists('article 5');
    $this->assertSession()->linkNotExists('article 6');

    // The moderator2 should only access to the articles created in the
    // past half the hour.
    $this->drupalLogin($this->moderator2);
    $this->drupalGet('article-moderation-queue');
    $this->assertSession()->linkExists('article 4');
    $this->assertSession()->linkExists('article 5');
    $this->assertSession()->linkExists('article 6');
    $this->assertSession()->linkNotExists('article 1');
    $this->assertSession()->linkNotExists('article 2');
    $this->assertSession()->linkNotExists('article 3');
  }

  /**
   * Test the argument settings.
   */
  public function testArgumentSettings():void {
    $this->drupalLogin($this->admin);
    $assert_session = $this->assertSession();

    // Settings were imported correclty on view setup.
    $this->drupalGet('admin/structure/views/nojs/handler/moderation_team_test/page_1/argument/moderation_team');
    $assert_session->statusCodeEquals(200);
    $assert_session->fieldValueEquals('options[role]', 'article_moderator');
    $assert_session->fieldValueEquals('options[date_field]', 'created');

    // Settings can be updated.
    $edit = [
      'options[role]' => 'authenticated',
      'options[date_field]' => $this->datetimeFiledName,
    ];
    $this->submitForm($edit, 'Apply');
    $this->drupalGet('admin/structure/views/nojs/handler/moderation_team_test/page_1/argument/moderation_team');
    $assert_session->statusCodeEquals(200);
    $assert_session->fieldValueEquals('options[role]', 'authenticated');
    $assert_session->fieldValueEquals('options[date_field]', $this->datetimeFiledName);
  }

  /**
   * Test queue with a config datetime field.
   */
  public function testConfigDateFieldOption(): void {
    $this->drupalLogin($this->admin);
    $assert_session = $this->assertSession();

    // Set the config datetime field.
    $this->drupalGet('admin/structure/views/nojs/handler/moderation_team_test/page_1/argument/moderation_team');
    $edit = [
      'options[date_field]' => $this->datetimeFiledName,
    ];
    $this->submitForm($edit, 'Apply');
    $this->submitForm([], 'Save');
    $assert_session->statusCodeEquals(200);

    // Moderation queue should work with the date field.
    $this->drupalLogin($this->moderator1);
    $this->drupalGet('article-moderation-queue');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->linkExists('article 1');
    $this->assertSession()->linkNotExists('article 4');

    $this->drupalLogin($this->moderator2);
    $this->drupalGet('article-moderation-queue');
    $this->assertSession()->linkExists('article 4');
    $this->assertSession()->linkNotExists('article 1');
  }

}
