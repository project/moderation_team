<?php

declare(strict_types=1);

namespace Drupal\moderation_team;

use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Class to view moderation data.
 */
final class ViewsData {

  use StringTranslationTrait;

  /**
   * Get the complete view of moderation data.
   */
  public function getData(): array {
    $data = [];

    $data['views']['moderation_team'] = [
      'title' => $this->t('Moderation team'),
      'argument' => [
        'title' => $this->t('Moderation team'),
        'id' => 'moderation_team',
      ],
    ];

    return $data;
  }

}
