<?php

declare(strict_types=1);

namespace Drupal\moderation_team\Plugin\views\argument;

use Drupal\Component\Utility\Html;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\Sql\SqlEntityStorageInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\Entity\Role;
use Drupal\user\RoleInterface;
use Drupal\views\Plugin\views\argument\NumericArgument;
use Drupal\views\Plugin\ViewsHandlerManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class for managing the moderation.
 *
 * @ingroup views_argument_handlers
 *
 * @ViewsArgument("moderation_team")
 */
final class ModerationTeam extends NumericArgument {

  private const SECONDS_IN_HOUR = 3600;
  private const DATE_FIELD_TYPES = [
    'created',
    'changed',
    'timestamp',
    'datetime',
  ];

  /**
   * Constructs a new ModerationTeam object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entityFieldManager
   *   The entity field manager.
   * @param \Drupal\views\Plugin\ViewsHandlerManager $viewsJoinPluginManager
   *   The view plugin manager.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    private EntityTypeManagerInterface $entityTypeManager,
    private EntityFieldManagerInterface $entityFieldManager,
    private ViewsHandlerManager $viewsJoinPluginManager,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): self {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager'),
      $container->get('plugin.manager.views.join')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions(): array {
    $options = parent::defineOptions();
    unset($options['not']);
    unset($options['break_phrase']);

    $options['role'] = ['default' => NULL];
    $options['date_field'] = ['default' => NULL];

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state): void {
    parent::buildOptionsForm($form, $form_state);
    unset($form['not']);
    unset($form['break_phrase']);

    $form['role'] = [
      '#type' => 'select',
      '#title' => $this->t('Role'),
      '#description' => $this->t('The moderator role.'),
      '#default_value' => $this->options['role'],
      '#options' => array_map(fn(RoleInterface $role) => Html::escape($role->label()), Role::loadMultiple()),
    ];

    $entity_type_fields = $this->entityFieldManager->getFieldMap()[$this->view->getBaseEntityType()->id()];
    $field_options = [];
    foreach ($entity_type_fields as $name => $map) {
      $type = $map['type'];
      if (\in_array($type, self::DATE_FIELD_TYPES, TRUE)) {
        $field_options[$name] = $name;
      }
    }

    $form['date_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Date field'),
      '#default_value' => $this->options['date_field'],
      '#options' => $field_options,
    ];

  }

  /**
   * {@inheritdoc}
   */
  public function query($group_by = FALSE): void {
    $this->ensureMyTable();

    $table = \array_key_first($this->query->tables);
    $uid = $this->getValue();
    $role = $this->options['role'];
    $field_name = $this->options['date_field'];

    $uids = $this->entityTypeManager->getStorage('user')->getQuery()
      ->condition('roles', $role, 'IN')
      ->condition('status', TRUE)
      ->accessCheck(TRUE)
      ->execute();
    $uids = \array_values($uids);
    if (\count($uids) === 0) {
      return;
    }

    $field_storage_definitions = $this->entityFieldManager->getFieldStorageDefinitions($this->view->getBaseEntityType()->id());
    $field_definition = $field_storage_definitions[$field_name];
    $entity_storage = $this->entityTypeManager->getStorage($this->view->getBaseEntityType()->id());
    \assert($entity_storage instanceof SqlEntityStorageInterface);
    $table_mapping = $entity_storage->getTableMapping();
    if ($table_mapping->requiresDedicatedTableStorage($field_definition)) {
      $field_table_name = $table_mapping->getFieldTableName($field_name);
      $field_table_name_value = $table_mapping->getFieldColumnName($field_definition, 'value');
      $date_field = "$field_table_name.$field_table_name_value";

      $entity_key_id = $this->entityTypeManager->getDefinition($this->view->getBaseEntityType()->id())->getKey('id');
      $definition = [
        'table' => $field_table_name,
        'field' => 'entity_id',
        'left_table' => $table,
        'left_field' => $entity_key_id,
      ];
      $join = $this->viewsJoinPluginManager->createInstance('standard', $definition);
      $this->query->addRelationship($field_table_name, $join, $table);
    }
    else {
      $date_field = "$table.$field_name";
    }

    if ($field_definition->getType() === 'datetime') {
      $date_field = "UNIX_TIMESTAMP($date_field)";
    }

    $sec = self::SECONDS_IN_HOUR;
    $step = $sec / \count($uids);
    $key = \array_search($uid, $uids, TRUE);
    $min = $key * $step;
    $max = ($key + 1) * $step;

    $snippet = "MOD($date_field, $sec) >= $min AND MOD($date_field, $sec) < $max";
    $this->query->addWhereExpression(0, $snippet);

    $this->view->element['#cache']['tags'][] = 'user_list';
  }

}
